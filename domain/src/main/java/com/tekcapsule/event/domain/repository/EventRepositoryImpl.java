package com.tekcapsule.event.domain.repository;

import com.tekcapsule.event.domain.model.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
public class EventRepositoryImpl implements EventDynamoRepository {


    @Autowired
    public EventRepositoryImpl() {
    }

    @Override
    public Event findBy(String code) {
        return null;
    }

    @Override
    public List<Event> findAll() {
        return null;
    }

    @Override
    public Event findBy(String code, String eventDate) {
        return null;
    }

    @Override
    public Event save(Event event) {
        return event;
    }

}
