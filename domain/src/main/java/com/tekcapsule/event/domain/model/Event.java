package com.tekcapsule.event.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tekcapsule.core.domain.AggregateRoot;
import com.tekcapsule.core.domain.BaseDomainEntity;
import lombok.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Event extends BaseDomainEntity implements AggregateRoot {

    private String code;
    private String eventDate;
    private String name;
    private String imageUrl;
    private String description;
    private String registrationUrl;
    private Boolean active;

}