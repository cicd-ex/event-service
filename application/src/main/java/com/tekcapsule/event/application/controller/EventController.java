package com.tekcapsule.event.application.controller;

import com.tekcapsule.core.domain.Origin;
import com.tekcapsule.event.application.config.AppConstants;
import com.tekcapsule.event.application.controller.input.CreateInput;
import com.tekcapsule.event.application.controller.input.DisableInput;
import com.tekcapsule.event.application.controller.input.GetInput;
import com.tekcapsule.event.application.controller.input.UpdateInput;
import com.tekcapsule.event.application.mapper.InputOutputMapper;
import com.tekcapsule.event.domain.command.CreateCommand;
import com.tekcapsule.event.domain.command.DisableCommand;
import com.tekcapsule.event.domain.command.UpdateCommand;
import com.tekcapsule.event.domain.model.Event;
import com.tekcapsule.event.domain.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController

@Slf4j
public class EventController {

    private final EventService eventService;

    public EventController(final EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/events")
    public ResponseEntity<Object>  create(@RequestBody CreateInput createInputMessage) {

        log.info(String.format("Entering create event Function - Event Code:%s",createInputMessage.getCode()));

        Origin origin = Origin.builder().build();

        CreateCommand createCommand = InputOutputMapper.buildCreateCommandFromCreateInput.apply(createInputMessage, origin);
        eventService.create(createCommand);
        Map<String, Object> responseHeader = new HashMap<>();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());

        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PostMapping("/events/{id}/disable")
    public ResponseEntity<Object>  disable(@RequestBody DisableInput disableInputMessage, @PathVariable String id) {

        log.info(String.format("Entering disable event Function - Event Code:%s", disableInputMessage.getCode()));

        Origin origin = Origin.builder().build();

        DisableCommand disableCommand = InputOutputMapper.buildDisableCommandFromDisableInput.apply(disableInputMessage, origin);
        eventService.disable(disableCommand);
        Map<String, Object> responseHeader = new HashMap<>();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());

        return new ResponseEntity<>(HttpStatus.OK);    }

    @GetMapping("events/")
    public ResponseEntity<Object>  getAll() {

        log.info("Entering get all events Function");

        List<Event> events = eventService.findAll();
        Map<String, Object> responseHeader = new HashMap<>();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.NOT_FOUND.value());

        return new ResponseEntity<>(HttpStatus.OK);    }

    @GetMapping("/events/{id}")
    public ResponseEntity<Object>  get(@RequestBody GetInput getInputMessage, @PathVariable String id) {

        log.info(String.format("Entering find by event Function - Event Code:%s}",  getInputMessage.getCode()));

        Event event = eventService.findBy(getInputMessage.getCode(),getInputMessage.getEventDate());
        Map<String, Object> responseHeader = new HashMap<>();
        if (event == null) {
            responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.NOT_FOUND.value());
            event = Event.builder().build();
        } else {
            responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());
        }
        return new ResponseEntity<>(HttpStatus.OK);    }

    @PatchMapping("/events/{id}")
    public ResponseEntity<Object> update(@RequestBody UpdateInput updateInputMessage, @PathVariable String id) {

        log.info(String.format("Entering update event Function - Event Code:%s",  updateInputMessage.getCode()));

        Origin origin = Origin.builder().build();

        UpdateCommand updateCommand = InputOutputMapper.buildUpdateCommandFromUpdateInput.apply(updateInputMessage, origin);
        eventService.update(updateCommand);
        Map<String, Object> responseHeader = new HashMap<>();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());

        return new ResponseEntity<>(HttpStatus.OK);
    }
}